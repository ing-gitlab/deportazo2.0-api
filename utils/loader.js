import fileSystem from 'fs';
import { Departamentos } from './../models';

const start = async () => {
    try {
        const filePath = `${__dirname}/colombia.json`;
        await fileSystem.readFile(filePath, 'utf8', async (err, content) => {
            try {
                if(!content) {
                    console.log(`No se encontro contenido en ${filePath}`);
                    return;
                }
                
                const data = JSON.parse(content);
                const departamentos = data.map(item => {
                    const { departamento, ciudades } = item;
                    return {
                        nombre: departamento,
                        ciudades
                    }
                });
                //crear departamento solo si no existe
                for (const departamento of departamentos) {
                    const { nombre } = departamento;
                    await Departamentos.findOneAndUpdate({ nombre }, departamento, { upsert: true });
                }
                return;            
            } catch (error) {
                console.log(error);
            }
        });
        console.log('Departamentos y ciudades cargados correctamente en la base de datos.');
    } catch (error) {
        console.log(error);
    }
}
 
module.exports = {
     start
}