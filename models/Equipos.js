
import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const equiposSchema = new Schema({
    nombre: String
})

const Equipos = mongoose.model('Equipos', equiposSchema);

module.exports = Equipos;