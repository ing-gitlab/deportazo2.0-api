
import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const departamentosSchema = new Schema({
    nombre: String,
    ciudades: [String],
})

const Departamentos = mongoose.model('Departamentos', departamentosSchema);

module.exports = Departamentos;