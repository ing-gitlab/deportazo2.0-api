
import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const CampeonatosSchema = new Schema({
    nombre: String,
    deporte: String,
    departamento: String,
    ciudad: String,
    zona: String,
    descripcion: String
});

const Campeonatos = mongoose.model('Campeonatos', CampeonatosSchema);

module.exports = Campeonatos;