import Equipos from './../../models/Equipos';

export default {
    Query: {
        Equipos: async () => {
            const equipos = await Equipos.find({});
            return equipos.map((x) => {
                x._id = x._id.toString();
                return x;
            })
        },
        Equipo: async (parent, args) => {
          const { id : _id } = args;
          const equipo = await Equipos.findOne({ _id });
          return equipo;
        }
    },
    Mutation: {
        crearEquipo: async (parent, args) => {
            const { input } = args;
            const equipo = await new Equipos(input).save();
            equipo._id = equipo._id.toString();
            return equipo;
        },
        actualizarEquipo: async (parent, args) => {
            const { input, id : _id } = args;
            const equipo = await Equipos.findOneAndUpdate({ _id }, input, { upsert: true });
            return equipo;
        }
    },
};
