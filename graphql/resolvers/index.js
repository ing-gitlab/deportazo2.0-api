import { mergeResolvers } from 'merge-graphql-schemas';
import campeonato from './campeonato';
import departamento from './departamento';
import equipo from './equipo';

const resolvers = [
  campeonato,
  departamento,
  equipo
];

export default mergeResolvers(resolvers);