import Campeonatos from './../../models/Campeonatos';

export default {
    Query: {
        Campeonatos: async () => {
            const campeonatos = await Campeonatos.find({});
            return campeonatos.map((x) => {
                x._id = x._id.toString();
                return x;
            })
        },
        Campeonato: async (parent, args) => {
          const { id : _id } = args;
          const campeonato = await Campeonatos.findOne({ _id });
          return campeonato;
        }
    },
    Mutation: {
        crearCampeonato: async (parent, args) => {
            const { input } = args;
            const campeonato = await new Campeonatos(input).save();
            campeonato._id = campeonato._id.toString();
            return campeonato;
        },
        actualizarCampeonato: async (parent, args) => {
            const { input, id : _id } = args;
            const campeonato = await Campeonatos.findOneAndUpdate({ _id }, input, { upsert: true });
            return campeonato;
        }
    },
};
