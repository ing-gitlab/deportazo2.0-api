import Departamentos from './../../models/Departamentos';

export default {
    Query: {
        Departamentos: async () => {
            return await Departamentos.find({});
        }
    }
};
