require('dotenv').config();

const {
    PORT = 4000,
    MONGO_HOST,
    MONGO_DATABASE,
    MONGO_USER,
    MONGO_PASSWORD,
    MONGO_REPLICA_SET,
    MONGO_AUTH_DATABASE
  } = process.env;

import express from 'express';
import bodyParser from 'body-parser';
import { graphiqlExpress, graphqlExpress } from 'apollo-server-express';
import mongoose from 'mongoose';
import cors from 'cors';
import loader from './utils/loader';

import { makeExecutableSchema } from 'graphql-tools';
import typeDefs from './graphql/types';
import resolvers from './graphql/resolvers';

const schema = makeExecutableSchema({ typeDefs, resolvers });

let MONGO_URL = `${MONGO_HOST}/${MONGO_DATABASE}`;
if (MONGO_REPLICA_SET) {
  MONGO_URL += `?replicaSet=${MONGO_REPLICA_SET}`;
}
if (MONGO_AUTH_DATABASE) {
  MONGO_URL += `${ MONGO_REPLICA_SET ? '&' : '?'}authSource=${MONGO_AUTH_DATABASE}`;
}
const mongoCredentials = MONGO_USER && MONGO_PASSWORD ? `${MONGO_USER}:${MONGO_PASSWORD}@${MONGO_URL}`: `${MONGO_URL}`;

console.log(`connecting to mongodb://${MONGO_HOST}/${MONGO_DATABASE}`);
mongoose.connect(`mongodb://${mongoCredentials}`, {
  reconnectTries: 30000,
  reconnectInterval: 1000,
  ssl: MONGO_REPLICA_SET ? true : false
});

const connection = mongoose.connection;
connection.on('connecting', () => console.log('Connecting to mongodb'));
connection.on('disconnecting', () => console.log('Disconnecting from mongodb'));
connection.on('disconnected', () => console.log('Disconnected from mongodb'));
connection.on('close', () => console.log('Mongodb connection closed'));
connection.on('reconnected', () => console.log('Mongodb connection reconnected'));
connection.on('error', err => console.log(err.message));
console.log('Connected successfully to mongodb');

//cargar departamentos con sus respectivas ciudades
loader.start();

const app = express();

// bodyParser is needed just for POST.
app.use('/graphql', cors(), bodyParser.json(), graphqlExpress({ schema }));

app.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql'}));

app.listen(PORT, () => {
  console.log('[Graphql] api running at port: ', PORT);
});
